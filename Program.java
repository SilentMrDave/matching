import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Program 
{
	public static int totalMen, totalWomen;
	private static Scanner sc;
	
	/**Prints preferences for a group of people*/
	public static void showPreferences(ArrayList<Person> people)
	{
		System.out.println("---------------------");
		for (int i = 0; i < people.size(); i++)
		{
			Person person = people.get(i);
			System.out.println("| " + person.getPrefixAndIndex() + ": " + person.getPrefs());
		}
		System.out.println("---------------------");
	}
	
	public static ArrayList<Person> randomizePeople(String name, int totalPeople, int totalOpposite)
	{
		ArrayList<Person> people = new ArrayList<Person>();
		for (int i = 0; i < totalPeople; i++)
		{
			people.add(new Person(name, i, totalOpposite));
		}
		return people;
	}
	//TODO: Validate preferences
	public static void manualPeople(String name, ArrayList<Person> people, int totalPeople, int totalOppsite)
	{
		ArrayList<Integer> prefs = new ArrayList<Integer>();
		System.out.println("Enter preferences as list of ints (Ex: 4 2 5 3)");
		for (int i = 0; i < totalPeople; i++)
		{				
			sc = new Scanner(System.in);
			
			System.out.print(name.substring(0).toLowerCase() + (i + 1) + " prefs: ");
			String line = sc.nextLine();
			String[] split = line.split(" ");
			for (String str : split)
			{
				int parsed = Integer.valueOf(str);
				prefs.add(parsed - 1);
			}
			people.add(new Person(name, i, new ArrayList<Integer>(prefs)));
			prefs.clear();
		}
	}
	/**Loads one of the 3 examples from the slides*/
	public static void loadExample(int example, ArrayList<Person> men, ArrayList<Person> women)
	{
		Integer[] prefs = {0,0,0};
		if (example == 1)
		{
			totalMen = totalWomen = 3;
			prefs = new Integer[] {1, 2, 0};
			men.add(new Person("Man", 0, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 2, 1};
			men.add(new Person("Man", 1, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 1, 2};
			men.add(new Person("Man", 2, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 1, 2};
			women.add(new Person("Woman", 0, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {1, 2, 0};
			women.add(new Person("Woman", 1, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {2, 1, 0};
			women.add(new Person("Woman", 2, new ArrayList<>(Arrays.asList(prefs))));
		}
		else if (example == 2)
		{
			totalMen = 5;
			totalWomen = 4;
			prefs = new Integer[] {0, 1, 2, 3};
			men.add(new Person("Man", 0, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {3, 1, 2, 0};
			men.add(new Person("Man", 1, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {3, 2, 0, 1};
			men.add(new Person("Man", 2, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 3, 2, 1};
			men.add(new Person("Man", 3, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 1, 3};
			men.add(new Person("Man", 4, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {1, 2, 0, 3, 4};
			women.add(new Person("Woman", 0, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {2, 0, 1, 3, 4};
			women.add(new Person("Woman", 1, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {4, 3, 0, 1, 2};
			women.add(new Person("Woman", 2, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 3, 4, 1, 2};
			women.add(new Person("Woman", 3, new ArrayList<>(Arrays.asList(prefs))));
		}
		else if (example == 3)
		{
			totalMen = totalWomen = 4;
			prefs = new Integer[] {0, 1};
			men.add(new Person("Man", 0, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 1, 2, 3};
			men.add(new Person("Man", 1, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {1, 0, 2};
			men.add(new Person("Man", 2, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {1, 0, 2, 3};
			men.add(new Person("Man", 3, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {3, 1, 0, 2};
			women.add(new Person("Woman", 0, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 2, 3};
			women.add(new Person("Woman", 1, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {0, 1, 3, 2};
			women.add(new Person("Woman", 2, new ArrayList<>(Arrays.asList(prefs))));
			prefs = new Integer[] {1, 3, 2};
			women.add(new Person("Woman", 3, new ArrayList<>(Arrays.asList(prefs))));
		}
	}
	
	public static void setTotals()
	{
		System.out.print("Total number of men: ");
		totalMen = sc.nextInt();
		System.out.print("Total number of women: ");
		totalWomen = sc.nextInt();
	}
	
	public static void printMatching(ArrayList<Match> matching)
	{
		if (matching.size() != 0)
		{
			System.out.print("Matching: ");
			for (Match match : matching)
			{
				System.out.print(match.getMatch() + " ");
			}
		}
	}
	
	public static void main(String[] args)
	{
		ArrayList<Person> men = new ArrayList<Person>();
		ArrayList<Person> women = new ArrayList<Person>();
		ArrayList<Match> matching = new ArrayList<Match>();
		sc = new Scanner(System.in);
		
		System.out.print("---Menu---\n"
						 + "1) Randomize\n"
						 + "2) Enter Manually\n"
						 + "3) Examples \n"
						 + "Enter choice: ");
		int option = sc.nextInt();
		switch (option)
		{
		case 1:
			setTotals();
			men = randomizePeople("Man", totalMen, totalWomen);
			women = randomizePeople("Woman", totalWomen, totalMen);
			break;
		case 2:
			setTotals();
			manualPeople("m", men, totalMen, totalWomen);
			manualPeople("w", women, totalWomen, totalMen);
			break;
		case 3:
			System.out.print("Which example (1-3)? ");
			int example = sc.nextInt();
			while(example < 1 || example > 3)
			{
				System.out.println("Invalid input. Try again (1-3): ");
				example = sc.nextInt();
			}
			loadExample(example, men, women);
			break;
		default:
			System.out.println("Invalid input");
			break;
		}
		
		showPreferences(men);
		showPreferences(women);
		do
		{
			matching.clear();
			for (Person man : men)
			{
				man.reset();
			}
			for (Person woman : women)
			{
				woman.reset();
			}
			System.out.print("\n---Choose Algorithm---\n"
					 + "1) MPDA\n"
					 + "2) WPDA\n"
					 + "3) Quit\n"
					 + "Enter choice: ");
			option = sc.nextInt();
			switch (option)
			{
			case 1:
				matching = PDA.matching(men, women);
				break;
			case 2:
				matching = PDA.matching(women, men);
				break;
			case 3:
				System.out.println("Quiting");
				break;
			default:
				System.out.println("Invalid input");
				break;	
			}			
			printMatching(matching);
		}while(option < 3);
	}
}
