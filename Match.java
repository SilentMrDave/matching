
public class Match 
{
	private Person man, woman;

	public Person getMan() {
		return man;
	}

	public Match(Person man, Person woman)
	{
		this.man = man;
		this.woman = woman;
	}
	
	public void setMan(Person man) 
	{
		this.man = man;
	}

	public Person getWoman() 
	{
		return woman;
	}

	public void setWoman(Person woman) 
	{
		this.woman = woman;
	}
	
	public String getMatch()
	{
		return ("(" + man.getPrefix() + (man.ID + 1) + ", " + woman.getPrefix() + (woman.ID + 1) + ")");
	}
}
