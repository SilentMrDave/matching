import java.util.ArrayList;


public class Hospital extends Person
{
	int capacity;
	Person[] residents;
	
	public Hospital(String name, int ID, ArrayList<Integer> prefs, int capacity) 
	{
		super(name, ID, prefs);
		this.capacity = capacity;
		residents = new Person[capacity];
	}

	public Hospital(String name, int ID, int total) 
	{
		super(name, ID, total);
		// TODO Auto-generated constructor stub
	}
	
	private int remainingCapacity()
	{
		int count = 0;
		for(int i = 0; i < capacity; i++)
		{
			if (residents[i] == null) count++;
		}
		return count;
	}
	
	private boolean hasMoreSpace()
	{
		return (remainingCapacity() > 0);
	}

	private boolean hasSpaceFor(Person person)
	{
		boolean wanted = hasMoreSpace();
		for (int i = 0; i < capacity && !wanted; i++)
		{
			if (residents[i] != null && hasHigherPreference(person, residents[i])) wanted = true;
		}
		return wanted;
	}
	
	public Person checkProps()
	{
		Person fanciest = null;
		if(fiance != null) fanciest = fiance;
		for(Person person : proposals)
		{
			if (getPrefValue(person) == -1)
			{
				continue;
			}
			else if (hasSpaceFor(person))
			{
				fanciest = person;
				sameFiance = false;
			}
		}
		if (fiance != null)
		{
			if (fiance != fanciest) fiance.engaged = false;
			else sameFiance = true;
		}
		if (fanciest != null)
		{
			fanciest.engaged = true;
			fiance = fanciest;
			engaged = true;
		}
		proposals.clear();
		return fiance;
	}
}
