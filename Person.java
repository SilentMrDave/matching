import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Person 
{
	int ID, currentPrefIndex = 0;
	String name = "";
	ArrayList<Integer> prefs = new ArrayList<Integer>();
	ArrayList<Person> proposals = new ArrayList<Person>();
	boolean engaged = false, sameFiance = false;
	Person fiance;
	
	public Person(String name, int ID, int total)
	{
		this.name = name;
		this.ID = ID;
		randomizePrefs(total);
	}
	
	public Person(String name, int ID, ArrayList<Integer> prefs)
	{
		this.name = name;
		this.ID = ID;
		this.prefs = prefs;
	}
	
	private void randomizePrefs(int max)
	{
		Random rand = new Random();
		prefs.clear();
		for (int i = 0; i < max; i++)
		{
			if(rand.nextInt() % 100 < 75)
			{
				prefs.add(i);
			}
		}
		Collections.shuffle(prefs);
	}
	
	public int getCurrentPref()
	{
		return prefs.get(currentPrefIndex);
	}
	
	public String getPrefs()
	{
		String listedPrefs = "";
		for (int pref : prefs)
		{
			listedPrefs += (pref + 1) + " ";
		}
		return listedPrefs;
	}
	
	public String getPrefix()
	{
		return name.substring(0, 1).toLowerCase();
	}
	
	public int getPrintIndex()
	{
		return ID + 1;
	}
	
	public String getPrefixAndIndex()
	{
		return getPrefix() + getPrintIndex();
	}
	
	public void propose(Person person)
	{
		person.proposals.add(this);
		if (currentPrefIndex < prefs.size()) currentPrefIndex++;
	}
	
	public int getPrefValue(Person person)
	{
		int pref = -1;
		for (int i = 0; i < prefs.size(); i++)
		{
			if (prefs.get(i) == person.ID) pref = i;
		}
		return pref;
	}
	
	/**
	 * True if Person a is preferred over b. False otherwise.
	 */
	public boolean hasHigherPreference(Person a, Person b)
	{
		if (getPrefValue(a) == -1 && getPrefValue(b) == -1) return false;
		else if (getPrefValue(b) == -1) return true;
		else if (getPrefValue(a) < getPrefValue(b)) return true;
		else return false;
	}
	
	public Person checkProps()
	{
		Person fanciest = null;
		if(fiance != null) fanciest = fiance;
		for(Person person : proposals)
		{
			if (getPrefValue(person) == -1)
			{
				continue;
			}
			else if (fanciest == null || hasHigherPreference(person, fanciest))
			{
				fanciest = person;
				sameFiance = false;
			}
		}
		if (fiance != null)
		{
			if (fiance != fanciest) fiance.engaged = false;
			else sameFiance = true;
		}
		if (fanciest != null)
		{
			fanciest.engaged = true;
			fiance = fanciest;
			engaged = true;
		}
		proposals.clear();
		return fiance;
	}
	
	public boolean hasMorePrefs()
	{
		if(prefs.size() > currentPrefIndex) return true;
		else return false;
	}
	
	public void reset()
	{
		fiance = null;
		engaged = false;
		sameFiance = false;
		currentPrefIndex = 0;
	}
}
