import java.util.ArrayList;
import java.util.Iterator;


public class PDA 
{
	static int rounds = 1;
	static ArrayList<Person> engaged = new ArrayList<Person>();
	static ArrayList<Person> unengaged = new ArrayList<Person>();
	static ArrayList<Match> matches = new ArrayList<Match>();
	
	private static void sendProposals(ArrayList<Person> proposers, ArrayList<Person> proposees)
	{
		Iterator<Person> it = unengaged.iterator();
		while (it.hasNext())
		{
			Person proposer = it.next();
			if (proposer.prefs.isEmpty() || !proposer.hasMorePrefs())
			{
				proposer.engaged = true;
				engaged.add(proposer);
				continue;
			}
			else
			{
				int currManPref = proposer.getCurrentPref();
				Person proposee = null;
				try
				{
					proposee = proposees.get(currManPref);
					proposer.propose(proposee);
					String message = proposer.getPrefixAndIndex() + " has proposed to "+ proposee.getPrefixAndIndex();
					System.out.println(message);
				}
				catch(IndexOutOfBoundsException e)
				{
					System.out.println("CurrManPref " + currManPref);
				}				
			}
		}
	}
	
	private static void reviewProposals(ArrayList<Person> proposees)
	{
		Iterator<Person> pit = proposees.iterator();
		while (pit.hasNext())
		{
			Person proposee = pit.next();
			if (!proposee.proposals.isEmpty())
			{
				Person chosen = proposee.checkProps();
				if (chosen != null && !proposee.sameFiance)	System.out.println(proposee.getPrefixAndIndex() + " has accepted "+ chosen.getPrefixAndIndex() + "'s proposal");
				else System.out.println(proposee.getPrefixAndIndex() + " did not accept any proposals");
				unengaged.remove(chosen);
				engaged.add(chosen);
			}
		}
	}
	
	private static void checkEngaged()
	{
		ArrayList<Person> toRemove = new ArrayList<>();
		for(Person proposer : engaged)
		{
			if (proposer != null && !proposer.engaged)
			{
				if (!unengaged.contains(proposer)) unengaged.add(proposer);				
				toRemove.add(proposer);
			}				
		}
		engaged.removeAll(toRemove);
	}
	
	public static ArrayList<Match> matching (ArrayList<Person> proposers, ArrayList<Person>proposees)
	{
		int rounds = 1;
		engaged = new ArrayList<Person>();
		unengaged = new ArrayList<Person>(proposers);
		matches = new ArrayList<Match>();
		
		while (!engaged.containsAll(proposers))
		{
			System.out.println("Round " + rounds++);
			sendProposals(proposers, proposees);
			reviewProposals(proposees);
			checkEngaged();
		}
		for (Person proposee : proposees)
		{
			if (proposee.fiance != null) matches.add(new Match(proposee.fiance, proposee));
		}
		return matches;
	}
}
